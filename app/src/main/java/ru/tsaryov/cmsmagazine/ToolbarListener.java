package ru.tsaryov.cmsmagazine;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.View;

public class ToolbarListener implements View.OnClickListener {
    @Override
    public void onClick(View view) {
        int clickedViewId = view.getId();

        switch (clickedViewId) {
            case R.id.side_button:
                View rootView = view.getRootView();
                DrawerLayout drawer = (DrawerLayout) rootView.findViewById(R.id.drawer_layout);
                if (!drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.openDrawer(GravityCompat.START);
                }

                break;
        }
    }
}
