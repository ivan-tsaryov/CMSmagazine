package ru.tsaryov.cmsmagazine;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by ivan on 02.07.16.
 */
public class DrawerListener implements View.OnClickListener {
    static int current_id = R.id.nav_news;

    Activity activity;
    ActionBar supportActionBar;

    public DrawerListener(Activity _activity, ActionBar _supportActionBar) {
        this.activity = _activity;
        this.supportActionBar = _supportActionBar;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        ToolbarListener tl = new ToolbarListener();
        ImageButton ib;

        switch (id) {
            case R.id.nav_news:
                setUpToolbar(R.layout.toolbar_with_sorting);
                makeTransition(view);

                ib = (ImageButton) activity.findViewById(R.id.side_button);
                ib.setOnClickListener(tl);

                ib = (ImageButton) activity.findViewById(R.id.sort_button);
                ib.setOnClickListener(tl);

                current_id = id;
                break;
            case R.id.nav_articles:
                setUpToolbar(R.layout.toolbar_without_sorting);
                makeTransition(view);

                ib = (ImageButton) activity.findViewById(R.id.side_button);
                ib.setOnClickListener(tl);

                current_id = id;
                break;
            case R.id.nav_blogs:
                setUpToolbar(R.layout.toolbar_with_sorting);
                makeTransition(view);

                ib = (ImageButton) activity.findViewById(R.id.side_button);
                ib.setOnClickListener(tl);

                ib = (ImageButton) activity.findViewById(R.id.sort_button);
                ib.setOnClickListener(tl);

                current_id = id;
                break;
            case R.id.nav_researches:
                setUpToolbar(R.layout.toolbar_with_sorting);
                makeTransition(view);

                ib = (ImageButton) activity.findViewById(R.id.side_button);
                ib.setOnClickListener(tl);

                ib = (ImageButton) activity.findViewById(R.id.sort_button);
                ib.setOnClickListener(tl);

                current_id = id;
                break;
            case R.id.nav_authors:
                setUpToolbar(R.layout.toolbar_without_sorting);
                makeTransition(view);

                ib = (ImageButton) activity.findViewById(R.id.side_button);
                ib.setOnClickListener(tl);
                current_id = id;
                break;
            case R.id.nav_favourites:
                setUpToolbar(R.layout.toolbar_without_sorting);
                makeTransition(view);

                ib = (ImageButton) activity.findViewById(R.id.side_button);
                ib.setOnClickListener(tl);

                current_id = id;
                break;
            case R.id.nav_settings:
                setUpToolbar(R.layout.toolbar_without_sorting);
                makeTransition(view);

                ib = (ImageButton) activity.findViewById(R.id.side_button);
                ib.setOnClickListener(tl);

                current_id = id;
                break;
        }
    }

    private void setUpToolbar(int resource) {
        // Настройка для кастомного тулбара
        View v = activity.getLayoutInflater().inflate(resource, null);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT);
        supportActionBar.setCustomView(v, layoutParams);
        supportActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
    }

    private void makeTransition(View view) {
        View rootView = view.getRootView();

        String title = ((TextView) view).getText().toString();
        ((TextView) rootView.findViewById(R.id.current_title)).setText(title);

        rootView.findViewById(current_id).setBackgroundResource(0);
        view.setBackgroundColor(Color.parseColor("#e9e9e9"));

        DrawerLayout drawer = (DrawerLayout) rootView.findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }
}
