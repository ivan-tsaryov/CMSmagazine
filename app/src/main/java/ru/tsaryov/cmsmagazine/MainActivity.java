package ru.tsaryov.cmsmagazine;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setUpDrawer();
        setUpToolbar();

//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();

//        ImageView b = (ImageView) findViewById(R.id.side_button);
//        b.setOnClickListener(new ToolbarListener(this));

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void setUpToolbar() {
        // Настройка для кастомного тулбара
        ActionBar ab = getSupportActionBar();
//        //ab.setHomeAsUpIndicator(R.drawable.ic_menu); // set a custom icon for the default home button
//        ab.setDisplayShowHomeEnabled(false); // show or hide the default home button
//        ab.setDisplayHomeAsUpEnabled(false); // show or hide back button at action bar
//        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
//        ab.setDisplayShowTitleEnabled(false); // disable the default title element here (for centered title)

        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.toolbar_with_sorting, null);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT);
        ab.setCustomView(v, layoutParams);
        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        ToolbarListener tl = new ToolbarListener();
        ImageButton ib;

        ib = (ImageButton) findViewById(R.id.side_button);
        ib.setOnClickListener(tl);

        ib = (ImageButton) findViewById(R.id.sort_button);
        ib.setOnClickListener(tl);
    }

    private void setUpDrawer() {
        DrawerListener dl = new DrawerListener(this, getSupportActionBar());
        TextView textView;

        textView = (TextView) findViewById(R.id.nav_news);
        textView.setOnClickListener(dl);

        textView = (TextView) findViewById(R.id.nav_articles);
        textView.setOnClickListener(dl);

        textView = (TextView) findViewById(R.id.nav_blogs);
        textView.setOnClickListener(dl);

        textView = (TextView) findViewById(R.id.nav_researches);
        textView.setOnClickListener(dl);

        textView = (TextView) findViewById(R.id.nav_authors);
        textView.setOnClickListener(dl);

        textView = (TextView) findViewById(R.id.nav_favourites);
        textView.setOnClickListener(dl);

        textView = (TextView) findViewById(R.id.nav_settings);
        textView.setOnClickListener(dl);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        System.out.println("ffffffff");
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_date) {
            System.out.println("ffffffff");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        return false;
    }
}
